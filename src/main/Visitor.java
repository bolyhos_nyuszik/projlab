package main;

public abstract class Visitor {
    private MapElement mapElement;

    Visitor(MapElement me) {
        mapElement = me;
    }

    public void HitBy(Trap t) {

    }

    public void HitBy(Target tar) {

    }

    public void HitBy(Wall o) {

    }

    public void HitBy(Hole h) {

    }

    public void HitBy(Field f) {

    }

    public void HitBy(Switch s) {

    }

    public boolean Push(Direction d) {
        return true;
    }

    public boolean Push(Direction d, Player byWhom) {
        return true;
    }

    public boolean Push(Direction d, Box byWhom) {
        return true;
    }

    public void Die() {
        mapElement.RemoveVisitor(this);
    }

    public void Blocked() {

    }

    public MapElement GetMapElement() {
        return mapElement;
    }
}
