package main;

public class Wall extends MapElement
{
    Wall(boolean de)
    {
        super(de);
    }

    public void Accept(Visitor v)
    {
        v.HitBy(this);
    }
}
