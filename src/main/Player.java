package main;

public class Player extends Visitor
{
    private int score;

    Player(MapElement me)
    {
       super(me);
       score = 0;
    }

    public void HitBy(Trap t)
    {
        GetMapElement().RemoveVisitor(this);
        t.AddVisitor(this);

        if (t.GetActivated())
        {
            Die();
            GetMapElement().RemoveVisitor(this);
        }
    }

    public void HitBy(Target tar)
    {
        GetMapElement().RemoveVisitor(this);
        tar.AddVisitor(this);
        if (tar.GetActive())
        {
            tar.Deactivate();
        }
    }

    public void HitBy(Wall o)
    {

    }   

    public void HitBy(Hole h)
    {
        GetMapElement().RemoveVisitor(this);
        Die();
    }

    public void HitBy(Field f)
    {
        GetMapElement().RemoveVisitor(this);
        f.AddVisitor(this);
    }

    public void HitBy(Switch s)
    {
        GetMapElement().RemoveVisitor(this);
        s.AddVisitor(this);
        s.ActivateTrap();
    }

    public boolean Push(Direction d)
    {
        MapElement startpoint = GetMapElement();
        MapElement target = GetMapElement().GetNeigbour(d);
        boolean success = target.PushVisitors(d, this);

        if (success)
        {
            target.Accept(this);
        }
        return (startpoint != target);
    }

    public boolean Push(Direction d, Player byWhom)
    {
        Push(d);
        return true;
    }

    public boolean Push(Direction d, Box byWhom)
    {
        MapElement startpoint = GetMapElement();
        MapElement target = GetMapElement().GetNeigbour(d);
        boolean success = target.PushVisitors(d, this);

        if (success)
        {
            target.Accept(this);
        }

        if (startpoint == GetMapElement())
        {
            Die();
        }

        return true;
    }

    public void AddPoint()
    {
        score++;
    }

    public void DecreasePoint()
    {
        score--;
    }

    public void SetPoint()
    {
        score = 0;
    }

    public void Die()
    {
        super.Die();
        //map.OneLessPlayer
    }

    public void Blocked()
    {

    }
}
