package main;

public class Hole extends MapElement
{
    Hole(boolean de)
    {
        super(de);
    }

    public void Accept(Visitor v)
    {
        v.HitBy(this);
    }
}
