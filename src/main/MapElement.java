package main;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class MapElement
{
    private List<Visitor> visitors;
    private boolean deadend;
    private Map<Direction, MapElement> neighbours;

    public MapElement(boolean de)
    {
        visitors = new ArrayList<Visitor>();
        deadend = de;
    }

    public void Accept(Visitor v)
    {

    }

    public MapElement GetNeigbour(Direction d)
    {
        if (neighbours.containsKey(d))
        {
            return neighbours.get(d);
        }
        return null;
    }

    public void AddVisitor(Visitor v)
    {
        visitors.add(v);
    }

    public void RemoveVisitor(Visitor v)
    {
        visitors.remove(v);
    }

    public boolean PushVisitors(Direction d, Visitor by)
    {
        return true;
    }

    public boolean PushVisitors(Direction d, Box b)
    {
        for(Visitor v : visitors)
        {
            if (!v.Push(d, b))
                return false;
        }
        return true;
    }

    public boolean PushVisitors(Direction d, Player p)
    {
        for(Visitor v : visitors)
        {
            if (!v.Push(d, p))
                return false;
        }
        return true;
    }

    public List<Visitor> GetVisitors()
    {
        return visitors;
    }
}
