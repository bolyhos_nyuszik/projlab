package main;

public class Trap extends MapElement
{
    private boolean activated;

    public Trap(boolean de, boolean a)
    {
        super(de);
        activated = a;
    }

    public void Activate()
    {
        activated = true;
    }

    public void Deactivate()
    {
        activated = false;
    }

    public void Accept(Visitor v)
    {
        v.HitBy(this);
    }

    public boolean GetActivated()
    {
        return activated;
    }
}
