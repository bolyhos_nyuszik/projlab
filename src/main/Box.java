package main;

public class Box extends Visitor {

    public Box(MapElement me) {
        super(me);
    }

    public void HitBy(Trap t) {
        GetMapElement().RemoveVisitor(this);
        t.AddVisitor(this);

        if (t.GetActivated()) {
            Die();
            GetMapElement().RemoveVisitor(this);
        }
    }

    public void HitBy(Target tar) {
        GetMapElement().RemoveVisitor(this);
        tar.AddVisitor(this);
        if (!tar.GetActive()) {
            tar.Activate();
        }
    }

    public void HitBy(Wall o) {

    }

    public void HitBy(Hole h) {
        GetMapElement().RemoveVisitor(this);
        Die();
    }

    public void HitBy(Field f) {
        GetMapElement().RemoveVisitor(this);
        f.AddVisitor(this);
    }

    public void HitBy(Switch s) {
        GetMapElement().RemoveVisitor(this);
        s.AddVisitor(this);
        s.ActivateTrap();
    }

    public boolean Push(Direction d) {
        MapElement startpoint = GetMapElement();
        MapElement target = GetMapElement().GetNeigbour(d);
        boolean success = target.PushVisitors(d, this);

        if (success) {
            target.Accept(this);
        }
        return (startpoint != GetMapElement());
    }

    public boolean Push(Direction d, Player byWhom) {
        return Push(d);
    }

    public boolean Push(Direction d, Box byWhom) {
        return Push(d);
    }
}
