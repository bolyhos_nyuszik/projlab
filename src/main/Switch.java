package main;

public class Switch extends MapElement
{
    private Trap trap;

    public Switch(boolean de, Trap t)
    {
        super(de);
        trap = t;
    }

    public void ActivateTrap()
    {
        trap.Activate();
    }

    public void DeactivateTrap()
    {
        trap.Deactivate();
    }

    public void Accept(Visitor v)
    {
        v.HitBy(this);
    }
}
