package main;

public class Target extends MapElement
{
    private boolean active;
    private Player player;

    public Target(boolean de, boolean a, Player p)
    {
        super(de);
        active = a;
        player = p;
    }

    public void Activate()
    {
        if (!active)
        {
            active = true;
            player.AddPoint();
        }
    }

    public void Deactivate()
    {
        if (active)
        {
            active = false;
            player.DecreasePoint();
        }
    }

    public boolean GetActive()
    {
        return active;
    }

    public void Accept(Visitor v)
    {
        v.HitBy(this);
    }


}
